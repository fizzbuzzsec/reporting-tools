def translate_risk(risk):
    # Translate Nessus risks (Critical, High, Medium, Low, None) to
    # BAE's High, Medium, Low, Info
    if risk == 'Critical':
        return 'High'
    elif risk == 'None':
        return 'Info'
    else:
        return risk


class Host:

    def __init__(self):
        self.ip = None
        self.name = None
        self.pingable = False
        self.ports = {}
        self.os = None

    def title(self, include_hostname=True):
        if self.ip and self.name and include_hostname:
            return "%s (%s)" % (self.ip, self.name)
        elif self.ip:
            return self.ip
        elif self.name:
            return self.name
        else:
            raise Exception("No hostname or ip")

    def _port_ident(self, port, proto):
        return str(port) + '/' + proto

    def has_port(self, port, proto):
        return self._port_ident(port, proto) in self.ports.keys()

    def get_port(self, port, proto):
        if not self.has_port(port, proto):
            return None
        else:
            return self.ports[self._port_ident(port, proto)]

    def add_port(self, portnum, proto, service=None, banner=None):
        port = self.get_port(portnum, proto)
        if not port:
            ident = self._port_ident(portnum, proto)
            port = {
                'port': int(portnum),
                'proto': proto,
                'service': [],
                'banner': []
            }
            self.ports[ident] = port

        if service:
            port['service'].append(service)

        if banner:
            port['banner'].append(banner)


class Plugin:

    def __init__(self):
        self.plugin_id = None
        self.synopsis = None
        self.description = None
        self.recommendation = None
        self.references = None
        self.cvss = None
        self.severity = None
        self.overwritten = False
        self.advisories = []
        self.instances = {}
        self.notes = 'Nessus description'

    def has_host(self, host):
        return host.ip in self.instances.keys()

    def add_instance(self, host, report_item):
        if not self.has_host(host):
            instance = PluginInstance(host)
            if '@protocol' in report_item.keys():
                instance.protocol = report_item['@protocol']
            if '@port' in report_item.keys():
                instance.port = report_item['@port']
            if 'plugin_output' in report_item.keys():
                instance.plugin_output = report_item['plugin_output']

            self.instances[host.ip] = instance

    def from_report_item(self, report_item):
        self.plugin_id = report_item['@pluginID']
        if 'plugin_name' in report_item.keys():
            self.plugin_name = report_item['plugin_name']
        else:
            self.plugin_name = 'Unknown?'
        self.synopsis = report_item['synopsis']
        self.description = report_item['description']
        self.recommendation = report_item['solution']
        if 'cve' in report_item.keys():
            cve = report_item['cve']
            if isinstance(cve, list):
                self.advisories = cve
            else:
                self.advisories = [cve]
        if 'see_also' in report_item.keys():
            self.references = report_item['see_also']
        else:
            self.references = 'n/a'
        if 'cvss_base_score' in report_item.keys():
            self.cvss = report_item['cvss_base_score']
        else:
            self.cvss = 'n/a'
        self.severity = translate_risk(report_item['risk_factor'])
        return self


class PluginInstance:

    def __init__(self, host):
        self.host = host
        self.port = None
        self.protocol = None
        self.plugin_output = None

    def format(self, include_hostname=True):
        output = self.host.title(include_hostname)
        if self.port and self.port != '0':
            output = "%s (%s/%s)" % (output, self.port, self.protocol)
        return output


def force_list(potential_list):
    """
    xmltodict will return leaf nodes as a dict if there is a single
    leaf and a list if there are multiple.

    return a list regardless of inpuit type
    """
    if isinstance(potential_list, list):
        return potential_list
    else:
        return [potential_list]


def keyexists(key, dict):
    """
    Check if a dictionary contains a key
    """
    return key in dict.keys()
