import argparse
import unicodecsv as csv
import dateutil.parser
import ast
import logging as log

parser = argparse.ArgumentParser(
    description='Parses a carbon black alert csv into a report')
parser.add_argument('csv', metavar='H',
                    help='The csv to parse')
parser.add_argument(
    '-d', help='The details file output', required=True)
parser.add_argument(
    '-s', help='The summary file output', required=True)
parser.add_argument('-v', '--verbose', action='store_const', const=True)
args = parser.parse_args()

level = 'INFO'
if args.verbose:
    level = 'DEBUG'
log.basicConfig(level=level, format='%(message)s')


alert_summary = {}


def get_event_time(row):
    return dateutil.parser.parse(row['created_time'])


def process_virustotal(row):
    # Clearly CB is using python here as the output is a python
    # array :-) so we'll just (Safely) eval it back to an array
    filenames = ast.literal_eval(row['observed_filename'])

    # CB outputs the filenames as 
    # <filename>.<hash>
    # So lets remove the hash (which is stored in watchlist_name)
    filenames_no_hash = []
    for filename in filenames:
        filenames_no_hash.append(
            filename.replace('.' + row['watchlist_name'], ''))
    return {
        'datetime': get_event_time(row),
        'description': row['description'] + '\n' + '\n'.join(filenames_no_hash),
        'username': row['username'],
        'hostname': row['hostname'],
        'severity': float(row['alert_severity'])
    }


def process_generic(row):

    severity = float(row['alert_severity'])

    if row['description'] == 'Suspicious Renamed cmd process':
        # Client-specific rule
        severity = 0

    return {
            'datetime': get_event_time(row),
            'description': row['description'],
            'username': row['username'],
            'hostname': row['hostname'],
            'severity': severity
        }


feed_processor_map = {
    'virustotal': process_virustotal
}


def calculate_severity(severity, description=''):

    if severity <= 10:
        return 'Info'
    elif severity <= 30:
        return 'Low'
    elif severity <= 50:
        return 'Medium'
    else:
        return 'High'


with open(args.csv, 'rb') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:

        if row['feed_name'] in feed_processor_map.keys():
            log.debug("Using custom processing for feed %s", row['feed_name'])
            summary = feed_processor_map[row['feed_name']](row)
        else:
            summary = process_generic(row)

        desc = summary['description']
        if desc not in alert_summary.keys():
            alert_summary[desc] = {
                'hosts': set(),
                'alert': desc,
                'severity': 0,
                'instances': []
            }
        alert_summary[desc]['hosts'].add(row['hostname'])

        if summary['severity'] > alert_summary[desc]['severity']:
            alert_summary[desc]['severity'] = summary['severity']
        alert_summary[desc]['instances'].append(summary)

with open(args.s, 'wb') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['ID', 'Alert', 'Hosts', 'Severity'])
    id_counter = 1
    for row in sorted(alert_summary.values(), key=lambda a: a['severity'], reverse=True):
        writer.writerow([id_counter, row['alert'],
                        len(row['hosts']), 
                        calculate_severity(row['severity'], row['alert'])])
        id_counter = id_counter+1

with open(args.d, 'wb') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['Description', 'Date/Time', 'Username', 'Hostname', 'Severity'])
    for row in sorted(alert_summary.values(), key=lambda a: a['severity'], reverse=True):
        for instance in sorted(row['instances'], key=lambda i: i['datetime']):
            username = instance['username']
            if not username:
                username = 'n/a'
            writer.writerow([
                row['alert'],
                instance['datetime'].strftime('%Y-%m-%d %H:%M:%S '),
                username,
                instance['hostname'].upper(),
                calculate_severity(instance['severity'], row['alert'])
            ])