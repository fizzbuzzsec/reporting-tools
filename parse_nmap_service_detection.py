#!/usr/bin/python2.7

import xmltodict
import argparse
from model import Host, force_list, keyexists
import logging as log
import unicodecsv as csv
import io

description = 'Parse nmap -sV results into a CSV.' + \
              ' Will import banners if available'
parser = argparse.ArgumentParser(description=description)
parser.add_argument('files', metavar='F', nargs='+',
                    help='The files to parse')
parser.add_argument('--output-disco', help='Output network table',
                    default=None)
parser.add_argument('--output-scope', help='Output scope table',
                    default=None)
parser.add_argument('--exclude-hosts', metavar='HOST',
                    help='Any hosts to exclude from the output',
                    nargs='+', default=[])
parser.add_argument('--without-names',
                    help='Do not report FQDNs in the report',
                    action='store_const', const=True, default=False)
args = parser.parse_args()

# A dictionary of hosts keyed by IP address
hosts = {}


def is_banner_script(script_output):
    """
    Returns True if the supplied nmap script taglet is a 'banner'
    script, currently if the script type is http-server-header
    or banner
    """
    return script_output['@id'] in ['http-server-header', 'banner']


def process_host(scan_host):
    """
    Translate the provided nmap host into a Host ready for reporting
    """

    # Try and extract the ipv4 address. If the host does not have
    # one, then drop it
    for address in force_list(scan_host['address']):
        if address['@addrtype'] == 'ipv4':
            host_ip = address['@addr']
    if not host_ip:
        log.error("Could not process host, no IP? %s", scan_host)
        return

    # Get the host from the host store, or create a new one
    if host_ip in hosts.keys():
        host = hosts[host_ip]
    else:
        host = Host()
        host.ip = host_ip
        hosts[host_ip] = host
        if keyexists('hostnames', scan_host):
            hostnames = scan_host['hostnames']
            if hostnames and 'hostname' in hostnames.keys():
                hostname_entries = force_list(hostnames['hostname'])
                for hostname in hostname_entries:
                    host.name = hostname['@name']
    # Determine the status
    status = scan_host['status']
    if status['@state'] != 'down' and status['@reason'] != 'user-set':
        host.pingable = True

    # Start processing the ports (if there are any)
    if keyexists('ports', scan_host):
        ports = scan_host['ports']
        if keyexists('port', ports):
            ports = force_list(ports['port'])
            for port in ports:
                state = port['state']
                if keyexists('@state', state) \
                        and state['@state'] == 'open':
                    port_name = ''
                    port_banner = ''

                    if keyexists('service', port):

                        service = port['service']

                        # Check if we have a name for this service
                        if keyexists('@name', service):
                            port_name = service['@name']
                            # If the service was found in a service table
                            # then it is unreliable. Append a '?'
                            if service['@method'] == 'table':
                                port_name = "%s?" % port_name
                            # Otherwise if the service is 'tcpwrapped'
                            # then we couldn't connect, so not worth reporting
                            elif port_name == 'tcpwrapped':
                                continue

                        if keyexists('script', port):
                            # Check for and process script outputs
                            script_outputs = force_list(port['script'])
                            for script_output in script_outputs:
                                if is_banner_script(script_output):
                                    # Add banner if there is one
                                    port_banner = script_output['@output']
                        if not port_banner and keyexists('@product', service):
                            port_banner = service['@product']

                    host.add_port(port['@portid'],
                                  port['@protocol'],
                                  port_name,
                                  port_banner)

    if keyexists('os', scan_host):
        os = scan_host['os']
        if os and keyexists('osmatch', os):
            osmatches = force_list(os['osmatch'])
            accuracy = 0
            os_name = None
            for osmatch in osmatches:
                if osmatch['@accuracy'] > accuracy:
                    accuracy = osmatch['@accuracy']
                    os_name = osmatch['@name']
            if os_name:
                host.os = os_name

# Process each nmap file
for file in args.files:
    print "Processing %s" % file
    with open(file, 'r') as file:
        scan = xmltodict.parse(file.read())
    scan_hosts = force_list(scan['nmaprun']['host'])
    for scan_host in scan_hosts:
        process_host(scan_host)

# Print out the network map to the CSV
if args.output_disco:
    with io.open(args.output_disco, 'wb') as output:
        writer = csv.writer(output, encoding="utf-8")
        for host in sorted(hosts.values(), key=lambda host: host.ip):
            if ((host.ports or host.pingable) and
                    (host.ip not in args.exclude_hosts and
                     host.name not in args.exclude_hosts)):
                firstrow = True
                ports = sorted(
                    host.ports.values(),
                    key=lambda port: port['port']
                )
                for port in ports:
                    row = []
                    if firstrow:
                        row.append(host.title(not args.without_names))
                        row.append('Yes' if host.pingable else 'No')
                        firstrow = False
                    else:
                        row.append('')
                        row.append('')
                    row.append('\n'.join(port['service']))
                    row.append("%d/%s" % (port['port'], port['proto']))

                    if port['banner']:
                        banner = '\n'.join(port['banner'])
                    else:
                        banner = 'Not detected'
                    row.append(banner)
                    writer.writerow(row)
                if not ports:
                    writer.writerow([
                        host.title(not args.without_names),
                        'Yes' if host.pingable else 'No',
                        'No services detected',
                        '-',
                        '-'])

if args.output_scope:
    with io.open(args.output_scope, 'wb') as output:
        writer = csv.writer(output, encoding="utf-8")
        for host in sorted(hosts.values(), key=lambda host: host.ip):
            if ((host.ports or host.pingable or host.os) and
                (host.ip not in args.exclude_hosts and
                 host.name not in args.exclude_hosts)):
                writer.writerow([
                    host.ip,
                    host.name if host.name else 'Unknown',
                    host.os if host.os else 'Unknown'])
