#!/usr/bin/python2.7

import unicodecsv as csv
import io
import xmltodict
import argparse
import ipaddress
from model import Host, Plugin, keyexists, force_list

plugins = {}
hosts = {}


def get_report_host_ip(report_host):
    """
    Attempt to get the IP of the host,
    otherwise return the nessus name (which may
    not be an IP Adress)
    """
    for prop in report_host['HostProperties']['tag']:
        if prop['@name'] == 'host-ip':
            return prop['#text']
    return report_host['@name']


# Configure the CLI parser
parser = argparse.ArgumentParser(description='Parse nessus results into a CSV')
parser.add_argument('files', metavar='F', nargs='+',
                    help='The files to parse')
parser.add_argument('--vulndb', help='CSV Vulnerability Database',
                    default='vulnerability DB.csv')
parser.add_argument('--output-vulns', help='Output file',
                    default='vulnerabilities.csv')
parser.add_argument('--output-network',
                    help='Output network table ' +
                    '(optional, prefer nmap if available)',
                    default=None)
parser.add_argument('--without-names',
                    help='Do not report FQDNs in the report',
                    action='store_const', const=True, default=False)
parser.add_argument('--without-plugin-output',
                    help='Do not create a column for plugin output',
                    action='store_const', const=True, default=False)
args = parser.parse_args()


def process_report_host(report_host):
    """
    Process a single host from the report
    """
    host_name = get_report_host_ip(report_host)

    # Get an existing host or create a new one
    if keyexists(host_name, hosts):
        host = hosts[host_name]
    else:
        host = Host()
        hosts[host_name] = host
        for prop in report_host['HostProperties']['tag']:
            if prop['@name'] == 'host-ip':
                host.ip = prop['#text']
            elif prop['@name'] == 'host-fqdn':
                host.name = prop['#text']
            elif prop['@name'] == 'operating-system':
                host.os = prop['#text']
        if not host.ip and not host.name:
            if '@name' in report_host.keys():
                # For some reason the IP/FQDN are not available
                # use the provided name from the scope (which may
                # be a IP or domain name)
                name = report_host['@name']
                try:
                    # Validate if this is an IP address
                    ipaddress.ip_address(name)
                    host.ip = name
                except ValueError:
                    host.name = name
            else:
                raise Exception("Could not calculate host name from %s", prop)

    if keyexists('ReportItem', report_host):
        for report_item in force_list(report_host['ReportItem']):
            plugin_id = report_item['@pluginID']
            if keyexists(plugin_id, plugins):
                plugin = plugins[plugin_id]
            else:
                plugin = Plugin().from_report_item(report_item)
                plugins[plugin_id] = plugin
            plugin.add_instance(host, report_item)

            # Process certain plugins for network discovery
            # Some plugins don't have output, so use synopsis instead
            if keyexists('plugin_output', report_item):
                output = report_item['plugin_output']
            else:
                output = report_item['synopsis']
            output = output.strip().replace('\n', '|')

            if keyexists('@pluginFamily', report_item):
                if report_item['@pluginFamily'] == 'Service detection':
                    host.add_port(
                        report_item['@port'],
                        report_item['@protocol'],
                        output
                    )
                else:
                    if keyexists('@port', report_item) and report_item['@port'] != '0':
                        host.add_port(
                            report_item['@port'],
                            report_item['@protocol'])
            if keyexists('@pluginID', report_item):
                if report_item['@pluginID'] == '10107':

                    output = output.replace(
                        'The remote web server type is :||', '')
                    host.add_port(
                        report_item['@port'],
                        report_item['@protocol'],
                        None,
                        output
                    )
                if report_item['@pluginID'] == '10180':
                    if 'The remote host replied to an ICMP echo packet' \
                       in report_item['plugin_output']:
                        host.pingable = True

# Read all the input files
for file in args.files:
    print "Processing %s" % file
    with open(file, 'r') as file:
        scan = xmltodict.parse(file.read())
    report = scan['NessusClientData_v2']['Report']
    for report_host in force_list(report['ReportHost']):
        process_report_host(report_host)

# Read the vulnerability database and overwrite the
# nessus findings
with open(args.vulndb, 'r') as csvfile:
    reader = csv.DictReader(csvfile, encoding="utf-8")
    for row in reader:
        plugin_id = row['Plugin ID']
        if plugin_id in plugins.keys():
            plugin = plugins[plugin_id]
            plugin.synopsis = row['Vulnerability']
            plugin.description = row['Description ']
            plugin.recommendation = row['Recommendation']
            # Nessus's advisories are probably more up to date
            # plugin.advisories = row['Advisories']
            plugin.notes = row['Notes']
            plugin.overwritten = True
            if row['Risk Level'] == '3 High':
                plugin.severity = 'High'
            elif row['Risk Level'] == '2 Med':
                plugin.severity = 'Medium'
            elif row['Risk Level'] == '1 Low':
                plugin.severity = 'Low'


def get_sev_integer(severity):
    """
    Return an integer for the severity for ordering purposes
    """
    if severity == 'High':
        return 50
    elif severity == 'Medium':
        return 30
    elif severity == 'Low':
        return 10
    else:
        return 0

plugins_sorted = sorted(plugins.values(),
                        key=lambda plugin: get_sev_integer(plugin.severity),
                        reverse=True)

# Write the output CSV
with io.open(args.output_vulns, 'wb') as output:
    writer = csv.writer(output, encoding="utf-8")
    writer.writerow([
        'Severity',
        'Synopsis',
        'Description',
        'Recommendation',
        'Advisories',
        'Affected Targets',
        'Plugin ID',
        'Plugin Name',
        'In Vuln DB?',
        'Notes',
        'Plugin Outputs'
    ])
    for plugin in plugins_sorted:
        if plugin.severity in ['High', 'Medium', 'Low']:
            targets_affected = []
            plugin_outputs = []
            advisories = '\n'.join(plugin.advisories)
            if not advisories:
                advisories = 'n/a'
            for ip, instance in plugin.instances.iteritems():
                targets_affected.append(
                    instance.format(not args.without_names))
                if not args.without_plugin_output:
                    plugin_outputs.append(
                        "%s:\n%s" % (instance.host.title(not args.without_names),
                                     instance.plugin_output))

            row = [
                plugin.severity,
                plugin.synopsis,
                plugin.description,
                plugin.recommendation,
                advisories,
                '\n'.join(targets_affected),
                plugin.plugin_id,
                plugin.plugin_name,
                str(plugin.overwritten),
                plugin.notes,
                '\n'.join(plugin_outputs)
            ]
            writer.writerow(row)

# If we're writing the network discovery do it now (note
# that NMAP output is almost always better)
if args.output_network:
    with io.open(args.output_network, 'wb') as output:
        writer = csv.writer(output, encoding="utf-8")
        for host in hosts.values():
            firstrow = True
            ports = sorted(
                host.ports.values(),
                key=lambda port: port['port']
            )
            for port in ports:
                row = []
                if firstrow:
                    row.append(host.title()
                               if not args.without_names else host.ip)
                    row.append('Yes' if host.pingable else 'No')
                    firstrow = False
                else:
                    row.append('')
                    row.append('')
                row.append('\n'.join(port['service']) if port['service'] else 'Unknown')
                row.append("%d/%s" % (port['port'], port['proto']))
                row.append('\n'.join(port['banner']) if port['banner'] else 'Unknown')
                writer.writerow(row)
